#!/bin/bash

cd quote-business/ && mvn -B clean install && cd ../
cd quote-web/ && mvn -B clean verify && cd ../
cd quote-api/ && mvn -B clean verify && cd ../
