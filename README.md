
# Auto Insurance Quote

Project to practice TDD in multi-module Groovy project with Bitbucket Pipelines

## Create remote repository

Create a git repository in Bitbucket  with the following data

Repository name: quote  
Access Level: public  
¿Create a README?:  No  
Control Version System: Git  
Project management: Issues management  
Language: Groovy

## Create the parent project

**Step 1.**  Create the parent project

```
mvn archetype:generate -DgroupId=com.example -DartifactId=quote -Dversion=0.0.1-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.0 -DinteractiveMode=false
```

> Now you can open the project in your favorite IDE as Maven Project

**Step 2.** (Optional) Add this README file to root folder

**Configure `pom.xml` of parent project**

**Step 3.** Replace the content in `pom.xml` with:

```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example</groupId>
    <artifactId>quote</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    
    <name>quote</name>
    <packaging>pom</packaging>

    <modules>
    </modules>
</project>
```

**Refactor parent project**

**Step 4.** Delete `src` folder

> src folder is not necessary for parent project

**Configure git for parent project**

**Step 5.** Generate a `.gitignore` file from
[https://gitignore.io](https://www.gitignore.io/api/maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode)
searching by maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode
and add it to the project

**Step 6.** Create git repository and commit changes

```
git init
git add :/
git commit
```

**Step 7.** Push local changes to BitBucket

```
git remote add origin git@bitbucket.org:juancarlosmolinaserrano/quote.git
git push origin master
```

> Replace remote url with your Bitbucket repository

## Create the business module

**Step 1.** Create new module inside the parent project from [https://start.spring.io/](https://start.spring.io/)
with the following data:

Group: com.example  
Artifact: quote-business  
Language: Groovy  
Description: Auto insurance quote business module (Library)  
Package: com.example.quote  
Dependencies: no dependencies

> In other fields use default values

**Configure `pom.xml` of business module**

**Step 2.** Remove the spring-boot-maven-plugin
 
> Repackage phase is not need for library business module

**Step 3** Add the following plugins to execute integration and coverage tests respectively

```
<plugin>
    <artifactId>maven-failsafe-plugin</artifactId>
    <version>2.21.0</version>
    <executions>
        <execution>
            <goals>
                <goal>integration-test</goal>
                <goal>verify</goal>
            </goals>
        </execution>
    </executions>
</plugin>
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.2</version>
    <executions>
        <execution>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>report</id>
            <phase>test</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

**Configure `pom.xml` of parent project**

**Step 4.** Add `<module>quote-business</module>` in modules tag

**Refactor business module**

**Step 5.** Move `src/main/groovy/com/example/quote/QuoteBusinessApplication.groovy` class
to `src/test/groovy/com/example/quote`

> To avoid conflicts with spring boot application. This class will be used only for start the testing environment

**Step 6.** Move `src/main/resources/application.properties` file to `src/test/resource`

> To avoid conflicts with a spring boot application. This file will be used only for load config for testing environment

**Step 7.** Delete auto-generated `src/test/groovy/com/example/quote/QuoteBusinessApplicationTests.groovy` class

> We will create test files when we need it

**Configure git to business module**

**Step 8.** Generate a `.gitignore` file from
[https://gitignore.io](https://www.gitignore.io/api/maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode)
searching by maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode
and add it to the business module

## Create the web module

**Step 1.** Create new module inside the parent project from [https://start.spring.io/](https://start.spring.io/) with:

Group: com.example  
Artifact: quote-web  
Language: Groovy  
Description: Auto insurance quote website module  
Package: com.example.quote  
Dependencies: web and devtools

> In other fields use default values

**Configure `pom.xml` of web module**

**Step 2.** Add quote-business dependency

```
<dependency>
    <groupId>com.example</groupId>
    <artifactId>quote-business</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

**Step 3.** Add the following plugins to execute integration and coverage tests respectively

```
<plugin>
    <artifactId>maven-failsafe-plugin</artifactId>
    <version>2.21.0</version>
    <executions>
        <execution>
            <goals>
                <goal>integration-test</goal>
                <goal>verify</goal>
            </goals>
        </execution>
    </executions>
</plugin>
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.2</version>
    <executions>
        <execution>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>report</id>
            <phase>test</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

**Configure `pom.xml` of parent project**

**Step 4.** Add `<module>quote-web</module>` in modules tag

**Refactor web module** 

**Step 5.** Delete auto-generated `src/test/groovy/com/example/quote/QuoteWebApplicationTests.groovy` class

> We will create test files when we need it

**Configure git of web module**

**Step 6.** Generate a `.gitignore` file from
[https://gitignore.io](https://www.gitignore.io/api/maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode)
searching by maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode
and add it to the web module

## Create a api module

**Step 1.** Create new module inside the parent project from [https://start.spring.io/](https://start.spring.io/) with:  
Group: com.example  
Artifact: quote-api  
Language: Groovy  
Description: Auto insurance quote RESTful api module  
Package: com.example.quote  
Dependencies: web and devtools

> In other fields use default values

**Configure `pom.xml` of api module**

**Step 2.** Add quote-business dependency

```
<dependency>
    <groupId>com.example</groupId>
    <artifactId>quote-business</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

**Step 3.** Add the following plugins to execute integration and coverage tests

```
<plugin>
    <artifactId>maven-failsafe-plugin</artifactId>
    <version>2.21.0</version>
    <executions>
        <execution>
            <goals>
                <goal>integration-test</goal>
                <goal>verify</goal>
            </goals>
        </execution>
    </executions>
</plugin>
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.2</version>
    <executions>
        <execution>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>report</id>
            <phase>test</phase>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

**Configure `pom.xml` of parent project**

**Step 4.** Add `<module>quote-api</module>` in modules tag

**Refactor api module** 

**Step 5.** Delete auto-generated src/test/groovy/com/example/quote/QuoteApiApplicationTests.groovy

> We will create test files when we need it

**Configure git for api module**

**Step 6.** Generate a `.gitignore` file from
[https://gitignore.io](https://www.gitignore.io/api/maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode)
searching by maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode
and add it to the api module

## Create the web tests module

**Step 1.** Create new module inside the parent project executing:

```
mvn archetype:generate -DgroupId=com.example -DartifactId=quote-web-tests -Dversion=0.0.1-SNAPSHOT -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.0 -DinteractiveMode=false
```


**Configure `pom.xml` of web tests module**

**Step 2.** Add Groovy dependency:

```
<dependency>
    <groupId>org.codehaus.groovy</groupId>
    <artifactId>groovy</artifactId>
    <version>2.5.4</version>
</dependency>
```

**Step 3.** Add gmavenplus-plugin

```
<build>
    <plugins>
        <plugin>
            <groupId>org.codehaus.gmavenplus</groupId>
            <artifactId>gmavenplus-plugin</artifactId>
            <version>1.5</version>
            <executions>
                <execution>
                    <goals>
                        <goal>addSources</goal>
                        <goal>addTestSources</goal>
                        <goal>generateStubs</goal>
                        <goal>compile</goal>
                        <goal>testGenerateStubs</goal>
                        <goal>testCompile</goal>
                        <goal>removeStubs</goal>
                        <goal>removeTestStubs</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

**Configure `pom.xml` of parent project**

**Step 4.** Review that quote-web-tests is added or add it

```
<modules>
    ...
    <module>quote-web-tests</module>
</modules>
```

**Refactor web tests module**

**Step 5.** Delete `src` folder

> We will files and folders when we need it

**Configure git for web tests module**

**Step 6.** Generate a `.gitignore` file from
[https://gitignore.io](https://www.gitignore.io/api/maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode)
searching by maven,gradle,eclipse,netbeans,intellij+all,visualstudiocode
and add it to the web tests module

## Configure Bitbucket pipelines

**Step 1.** Create the `bitbucket-pipelines.yml` file in root project with the following content:

```
# This is a sample build configuration for Java (Maven).
# Check our guides at https://confluence.atlassian.com/x/zd-5Mw for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image: maven:3.3.9

pipelines:
  default:
    - step:
        caches:
          - maven
        script: # Modify the commands below to build your repository.
          - chmod +x verify.sh && ./verify.sh
```

**Step 2.** Create the `verify.sh` file with the following content:

```
#!/bin/bash

cd quote-business/ && mvn -B clean install && cd ../
cd quote-web/ && mvn -B clean verify && cd ../
cd quote-api/ && mvn -B clean verify && cd ../
```

**Step 3.** Commit and push changes to Bitbucket

**Step 4.** Open Bitbucket in your browser, go to Pipelines section and enable pipelines

**Step 5.** In your local repository execute:

```
git fetch
git merge origin/master
```

> To get the remote commit with the pipelines activation
