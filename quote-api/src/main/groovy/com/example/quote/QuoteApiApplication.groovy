package com.example.quote

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class QuoteApiApplication {

	static void main(String[] args) {
		SpringApplication.run(QuoteApiApplication, args)
	}

}
